<?php

namespace AKPranga\ApiClient\Providers;

use AKPranga\ApiClient\Classes\Curl;
use AKPranga\ApiClient\Contracts\CurlInterface;
use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Route;

/**
 * Class Provider
 *
 * @package AKPranga\ApiClient\Providers
 */
class Provider extends ServiceProvider
{

    protected $namespace = 'AKPranga\ApiClient\Http\Controllers';

    public function boot(): void
    {
        $this->registerResources();
        $this->publishes([
            __DIR__ . '/../../config/bm_api.php' => config_path('bm_api.php'),
            __DIR__ . '/../../resources/views'   => resource_path('views/vendor/client-api-view'),
        ]);
    }

    public function register(): void
    {
        $this->mapBinding();
        $this->mapWebRoutes();
        $this->mergeConfigFrom(__DIR__ . '/../../config/bm_api.php', 'bm_api');
    }

    protected function mapBinding(): void
    {
        $this->app->bind(CurlInterface::class, function (Application $app) {
            return $app->make(Curl::class);
        });
    }

    /**
     * @return void
     */
    public function registerResources(): void
    {
        $this->loadViewsFrom(__DIR__ . '/../../resources/views', 'client-api-view');
    }

    protected function mapWebRoutes(): void
    {
        Route::group([
            'prefix'     => config('bm_api.prefix', 'bm_api'),
            'namespace'  => $this->namespace,
            'middleware' => 'web'
        ], function () {
            $this->loadRoutesFrom(__DIR__ . '/../../routes/web.php');
        });
    }
}
