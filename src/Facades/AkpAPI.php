<?php

namespace AKPranga\ApiClient\Facades;

use AKPranga\ApiClient\Classes\RestApi;
use Illuminate\Support\Facades\Facade;

class AkpAPI extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return RestApi::class;
    }
}
