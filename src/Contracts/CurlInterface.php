<?php

namespace AKPranga\ApiClient\Contracts;

interface CurlInterface
{
    public function sendRequest(string $url, string $method, string $data = '');

    public function setOption(string $option, $value);

    public function setOptions(array $options = []);

    public function setUrl(string $url): self;

    public function setMethod(string $method): self;

    public function setData(string $data);
}
