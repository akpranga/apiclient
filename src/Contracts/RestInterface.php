<?php

namespace AKPranga\ApiClient\Contracts;

interface RestInterface
{
    public function get(string $url);

    public function post(string $url, string $data);

    public function put(string $url, string $data);

    public function delete(string $url);
}
