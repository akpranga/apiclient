<?php

namespace AKPranga\ApiClient\Http\Controllers;

use AKPranga\ApiClient\Facades\AkpAPI;
use AKPranga\ApiServer\Http\Requests\ItemRequest;
use Illuminate\Routing\Controller;

class ItemController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $items = json_decode(AkpAPI::get(config('bm_api.api') . '/items'), true);

        return view('client-api-view::items.index', compact('items'));
    }

    /**
     * @param $itemId
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($itemId)
    {
        $item = AkpAPI::get(config('bm_api.api') . '/items/' . $itemId);

        if (!$item) {
            abort(404);
        }

        $item = json_decode($item, true);

        return view('client-api-view::items.edit', compact('item'));
    }

    /**
     * @param $itemId
     * @param ItemRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($itemId, ItemRequest $request): \Illuminate\Http\RedirectResponse
    {
        $item = AkpAPI::get(config('bm_api.api') . '/items/' . $itemId);

        if (!$item) {
            abort(404);
        }

        $item = AkpAPI::put(config('bm_api.api') . '/items/' . $itemId, json_encode([
            'name'   => $request->input('name'),
            'amount' => $request->input('amount')
        ]));

        if (!$item) {
            abort(500);
        }

        return redirect()->route('front_item.index')->with('success', 'Pomyślnie zaktualizowano produkt');
    }

    public function destroy($itemId)
    {
        $item = AkpAPI::delete(config('bm_api.api') . '/items/' . $itemId);

        if (!$item) {
            abort(500);
        }

        return redirect()->route('front_item.index')->with('success', 'Pomyślnie usunięto produkt');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('client-api-view::items.create');
    }

    /**
     * @param ItemRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ItemRequest $request): \Illuminate\Http\RedirectResponse
    {
        $item = AkpAPI::post(config('bm_api.api') . '/items', json_encode([
            'name'   => $request->input('name'),
            'amount' => $request->input('amount')
        ]));

        if (!$item) {
            abort(500);
        }

        return redirect()->route('front_item.index')->with('success', 'Pomyślnie dodano produkt');
    }

    public function available($items = 1)
    {
        $items = AkpAPI::get(config('bm_api.api') . '/items/available/' . $items);
        if ($items) {
            $items = json_decode($items, true);
        }
        return view('client-api-view::items.index', compact('items'));
    }

    public function notAvailable()
    {
        $items = AkpAPI::get(config('bm_api.api') . '/items/not_available');
        if ($items) {
            $items = json_decode($items, true);
        }
        return view('client-api-view::items.index', compact('items'));
    }
}
