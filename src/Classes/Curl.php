<?php

namespace AKPranga\ApiClient\Classes;

use AKPranga\ApiClient\Contracts\CurlInterface;

class Curl implements CurlInterface
{
    /**
     * @var resource
     */
    private $curl;

    public function __construct()
    {
        $this->curl = curl_init();
    }

    /**
     * @param string $url
     * @param string $method
     * @param string $data
     *
     * @return mixed
     */
    public function sendRequest(string $url, string $method, string $data = '')
    {
        $this->setOption(CURLOPT_RETURNTRANSFER, true);
        $this->setOption(CURLOPT_HEADER, false);
        $this->setUrl($url);
        $this->setMethod($method);
        $this->setData($data);
        $this->setContentType('application/json');
        $response = curl_exec($this->curl);
        curl_close($this->curl);

        $this->curl = curl_init();
        return $response;
    }

    /**
     * @param array $options
     *
     * @return void
     */
    public function setOptions(array $options = []): void
    {
        foreach ($options as $option => $value) {
            curl_setopt($this->curl, $option, $value);
        }
    }

    /**
     * @param $option
     * @param $value
     *
     * @return void
     */
    public function setOption(string $option, $value): void
    {
        curl_setopt($this->curl, $option, $value);
    }

    /**
     * @param string $url
     *
     * @return Curl
     */
    public function setUrl(string $url): CurlInterface
    {
        $this->setOption(CURLOPT_URL, $url);

        return $this;
    }

    /**
     * @param string $method
     *
     * @return CurlInterface
     */
    public function setMethod(string $method): CurlInterface
    {
        $method = strtoupper($method);
        if (!\in_array($method, ['GET', 'POST', 'PUT', 'PATCH', 'DELETE'])) {
            throw new \InvalidArgumentException("Method not supported: {$method}");
        }

        $this->setOption(CURLOPT_CUSTOMREQUEST, $method);

        return $this;
    }

    /**
     * @param string $data
     */
    public function setData(string $data): void
    {
        $this->setOption(CURLOPT_POSTFIELDS, $data);
    }

    /**
     * @param string $contentType
     */
    public function setContentType(string $contentType): void
    {
        $this->setOption(CURLOPT_HTTPHEADER, ['Content-Type:' . $contentType]);
    }
}
