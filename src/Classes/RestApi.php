<?php

namespace AKPranga\ApiClient\Classes;

use AKPranga\ApiClient\Contracts\CurlInterface;
use AKPranga\ApiClient\Contracts\RestInterface;

class RestApi implements RestInterface
{
    /**
     * @var CurlInterface
     */
    private $curl;
    /**
     * @var string
     */
    private $urlRestApiServer;

    /**
     * RestClient constructor.
     *
     * @param CurlInterface $curl
     */
    public function __construct(CurlInterface $curl)
    {
        $this->curl             = $curl;
    }

    /**
     * @param string $url
     *
     * @return mixed
     */
    public function get(string $url)
    {
        return $this->curl->sendRequest($this->urlRestApiServer . $url, 'GET');
    }

    /**
     * @param string $url
     * @param string $data
     *
     * @return mixed
     */
    public function post(string $url, string $data)
    {
        return $this->curl->sendRequest($this->urlRestApiServer . $url, 'POST', $data);
    }

    /**
     * @param string $url
     * @param string $data
     *
     * @return mixed
     */
    public function put(string $url, string $data)
    {
        return $this->curl->sendRequest($this->urlRestApiServer . $url, 'PUT', $data);
    }

    /**
     * @param string $url
     *
     * @return mixed
     */
    public function delete(string $url)
    {
        return $this->curl->sendRequest($this->urlRestApiServer . $url, 'DELETE');
    }

    /**
     * @param string $contentType
     */
    public function setContentType(string $contentType): void
    {
        $this->curl->setContentType($contentType);
    }

    /**
     * @return string
     */
    public function getApiUrl(): string
    {
        return $this->urlRestApiServer;
    }

    /**
     * @param string $apiUrl
     *
     * @return $this
     */
    public function setApiUrl(string $apiUrl): self
    {
        $this->urlRestApiServer = $apiUrl;
        return $this;
    }
}
