RestApi Client Package for Laravel 5
====================================

Requirments
====================================
* PHP >= 7.1
* cURL Extension
* Laravel >= 5.6

Installation
============

        composer require akpranga/apiclient
       
Configuration
=============

To get started, you'll need to publish configuration file (`config/bm_api.php`) and change URL to RestApi Server

        php artisan vendor:publish --provider="AKPranga\ApiClient\Providers\Provider"


Credits
=======
* Adrian Pranga