@extends('client-api-view::layout')

@section('title') Lista dzielnic @append

@section('body')
<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="float-left">
                <h1>Produkty</h1>
            </div>
            <div class="float-right">
                <a href="{{ route('front_item.create') }}">
                    <i class="fas fa-plus-square"></i> Dodaj nowy produkt
                </a>
            </div>
        </div>
    </div>

    <div class="row" style="padding-bottom: 30px">
        <div class="col-4">
            <a href="{{ route('item_available') }}">Dostępne na stanie</a>
        </div>
        <div class="col-4">
            <a href="{{ route('item_not_available') }}">Niedostępne na stanie</a>
        </div>
        <div class="col-4">
            <a href="{{ route('item_available', ['amount' => 5]) }}">Więcej niż 5</a>
        </div>
    </div>

    <div class="row">
        <table class="table">
            <thead>
            <tr>
                <th>Id</th>
                <th>Nazwa</th>
                <th>Ilość</th>
                <th>Akcje</th>
            </tr>
            </thead>
            <tbody>
            @forelse($items as $item)
            <tr>
                <td>{{ $item['id'] }}</td>
                <td>{{ $item['name'] }}</td>
                <td>{{ $item['amount'] }}</td>
                <td>
                    <a class="btn btn-primary" href="{{ route('front_item.edit', ['item' => $item['id']]) }}">
                        <i class="fas fa-edit"></i>
                    </a>
                    @include('client-api-view::items._delete_form')
                </td>
            </tr>
            @empty
            <tr>
                <td colspan="6">Brak rekordów</td>
            </tr>
            @endforelse
            </tbody>
        </table>
    </div>
</div>
@endsection