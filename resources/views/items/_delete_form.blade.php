<form method="post" action="{{ route('front_item.destroy', ['id' => $item['id']]) }}" onsubmit="return confirm('Czy na pewno chcesz usunąć?');">
    <input type="hidden" name="_method" value="DELETE">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <button class="btn btn-danger">
        <i class="fas fa-trash-alt"></i>
    </button>
</form>