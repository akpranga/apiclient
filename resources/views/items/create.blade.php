@extends('client-api-view::layout')

@section('body')
    <div class="container">
        <div class="row">
            <h1>Dodaj nowy produkt</h1>
        </div>

        <div class="row">
            <form method="post" action="{{ route('front_item.store') }}">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="name">Nazwa:</label>
                    <input type="text" name="name" class="form-control" value=""/>
                </div>
                <div class="form-group">
                    <label for="amount">Ilość</label>
                    <input type="text" name="amount" class="form-control" value="" />
                </div>
                <button type="submit" class="btn btn-primary">Zapisz</button>
            </form>
        </div>
    </div>
@endsection