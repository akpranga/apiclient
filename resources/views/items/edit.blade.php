@extends('client-api-view::layout')

@section('body')
    <div class="container">
        <div class="row">
            <h1>Edycja produkt: {{ $item['name'] }}</h1>
        </div>

        <div class="row">
            <form method="post" action="{{ route('front_item.update', ['item' => $item['id']]) }}">
                <input type="hidden" name="_method" value="PUT" />
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="name">Nazwa:</label>
                    <input type="text" name="name" class="form-control" value="{{ $item['name'] ?? '' }}"/>
                </div>
                <div class="form-group">
                    <label for="amount">Ilość</label>
                    <input type="text" name="amount" class="form-control" value="{{ $item['amount'] ?? '' }}" />
                </div>
                <button type="submit" class="btn btn-primary">Zapisz</button>
            </form>
        </div>
    </div>
@endsection