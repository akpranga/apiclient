<?php

return [
    'api' => env('APP_URL', 'http://localhost') . '/api',
    'prefix' => 'bm_api'
];
