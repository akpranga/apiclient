<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'ItemController@index')->name('item_index');
Route::resource('items', 'ItemController', [
    'names' => [
        'index'   => 'front_item.index',
        'store'   => 'front_item.store',
        'create'  => 'front_item.create',
        'edit'    => 'front_item.edit',
        'update'  => 'front_item.update',
        'destroy' => 'front_item.destroy',
        'show'    => 'front_item.show'
    ]
]);
Route::get('/available/{amount?}', 'ItemController@available')->name('item_available');
Route::get('/not_available', 'ItemController@notAvailable')->name('item_not_available');
